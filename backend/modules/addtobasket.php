<? if ($_RAW['type'] == 'basket') {
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

    //добавление в корзину
    if ($_RAW['action'] == 'add') {

        if (isset($_RAW['id'])) {

            if (isset($_RAW['quantity'])) {
                $productID = intval($_RAW['id']);
                $quantity = intval($_RAW['quantity']);

                if (CModule::IncludeModule("sale") && CModule::IncludeModule("catalog")) {
                    Add2BasketByProductID($productID, $quantity, false);
                    $answer = array('status'=>true);

                    /*$arID = array();
                    $arBasketItems = array();
                    $dbBasketItems = CSaleBasket::GetList(
                        array(
                            "NAME" => "ASC",
                            "ID" => "ASC"
                        ),
                        array(
                            "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                            "LID" => SITE_ID,
                            "ORDER_ID" => "NULL"
                        ),
                        false,
                        false,
                        array("ID", "CALLBACK_FUNC", "MODULE", "PRODUCT_ID", "QUANTITY", "PRODUCT_PROVIDER_CLASS")
                    );
                    while ($arItems = $dbBasketItems->Fetch()) {
                        if ('' != $arItems['PRODUCT_PROVIDER_CLASS'] || '' != $arItems["CALLBACK_FUNC"]) {
                            CSaleBasket::UpdatePrice($arItems["ID"],
                                $arItems["CALLBACK_FUNC"],
                                $arItems["MODULE"],
                                $arItems["PRODUCT_ID"],
                                $arItems["QUANTITY"],
                                "N",
                                $arItems["PRODUCT_PROVIDER_CLASS"]
                            );
                            $arID[] = $arItems["ID"];
                        }
                    }
                    if (!empty($arID)) {
                        $dbBasketItems = CSaleBasket::GetList(
                            array(
                                "NAME" => "ASC",
                                "ID" => "ASC"
                            ),
                            array(
                                "ID" => $arID,
                                "ORDER_ID" => "NULL"
                            ),
                            false,
                            false,
                            array("ID", "PRODUCT_ID", "QUANTITY", "CAN_BUY", "PRICE", "WEIGHT", "PRODUCT_PROVIDER_CLASS", "NAME")
                        );
                        while ($arItems = $dbBasketItems->Fetch()) {
                            $arBasketItems[] = $arItems;
                        }
                    }

                    echo "<pre>";
                    print_r($dbBasketItems);
                    echo "</pre>";*/
                }
            } else {
                $answer = array('status' => false, 'msg' => 'No quantity');
            }
        } else {
            $answer = array('status' => false, 'msg' => 'Wrong product ID');
        }
    }

}
?>