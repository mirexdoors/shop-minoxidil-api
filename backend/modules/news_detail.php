<?
if ($_RAW['type'] == 'news_detail') {
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
    CModule::IncludeModule("iblock");

    if (isset($_RAW['id']) && !empty($_RAW['id'])) {
        //Кэширование
        $obCache = new CPHPCache();

        $cache_time = 3600 * 24 * 14;
        $cacheID = serialize(array($_RAW['type'], $_RAW['id']));
        $cachePath = '/mobile/news_detail';

        if ($obCache->InitCache($cache_time, $cacheID, $cachePath))// Если кэш валиден
        {
            $vars = $obCache->GetVars();
            $answer = $vars['answer'];

        } elseif ($obCache->StartDataCache()) {
            $IBLOCK_ID = 1;
            $id = intval($_RAW['id']);
            $arResult = array();

            $arSelect = Array("ID", "NAME", "DETAIL_PICTURE", "DATE_CREATE", "DETAIL_TEXT");
            $arFilter = Array("IBLOCK_ID" => $IBLOCK_ID, "ID" => $id, "ACTIVE" => "Y");
            $res = CIBlockElement::GetList(Array("ID" => "DESC"), $arFilter, false, false, $arSelect);

            if ($ob = $res->GetNext()) {

                if (!empty($ob['DETAIL_PICTURE'])) {
                    $src = CFile::GetPath($ob['DETAIL_PICTURE']);
                    $ob['DETAIL_PICTURE'] = $src;
                }
                
                $arResult = array();
                $arResult['ID'] = $ob['ID'];
                $arResult['NAME'] = $ob['NAME'];
                $arResult['DETAIL_PICTURE'] = $ob['DETAIL_PICTURE'];
                $arResult['DETAIL_TEXT'] = html_entity_decode(strip_tags($ob['DETAIL_TEXT']));
                $arResult['DATE_CREATE'] = $ob['DATE_CREATE'];
              
            }

           
            if (!empty($arResult)) {
                $answer = $arResult;
                $obCache->EndDataCache(// Сохраняем переменные в кэш.
                    array('answer' => $answer)
                );
            } else {
                $answer = array('status' => false, 'msg' => 'I have no news with this ID');
            }
        }
    } else {
        $answer = array('status' => false, 'msg' => 'BAD query. No news ID');
    }
}
?>