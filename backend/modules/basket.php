<? if ($_RAW['type'] == 'basket') {
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

    //добавление в корзину
    if ($_RAW['action'] == 'add') {

        if (isset($_RAW['id'])) {

            if (isset($_RAW['quantity'])) {
                $productID = intval($_RAW['id']);
                $quantity = intval($_RAW['quantity']);

                if (CModule::IncludeModule("sale") && CModule::IncludeModule("catalog")) {
                    $basketItemId = Add2BasketByProductID($productID, $quantity, false);
                    $answer = array('status' => true, 'basketItemId' => $basketItemId);
                }
            } else {
                $answer = array('status' => false, 'msg' => 'No quantity');
            }
        } else {
            $answer = array('status' => false, 'msg' => 'Wrong product ID');
        }
    } elseif ($_RAW['action'] == 'delete') {  //удаление из корзины
        if (isset($_RAW['basketId'])) {
            if (CSaleBasket::Delete(intval($_RAW['basketId']))) {
                $answer = array('status' => true, 'msg' => 'Product was deleted');
            }
        } else {
            $answer = array('status' => false, 'msg' => 'Wrong basket ID');
        }
    } elseif ($_RAW['action'] == 'getItems') {
        if (isset($_RAW['user_id'])) {
            CModule::IncludeModule("sale") && CModule::IncludeModule("catalog");
            $userId = intval($_RAW['user_id']);
            $arFUser = CSaleUser::GetList(array('USER_ID' => $userId));

            $arID = array();

            $arBasketItems = array();

            $dbBasketItems = CSaleBasket::GetList(
                array(
                    "NAME" => "ASC",
                    "ID" => "ASC"
                ),
                array(
                    "FUSER_ID" =>  $arFUser,
                    "LID" => SITE_ID,
                    "ORDER_ID" => "NULL"
                ),
                false,
                false,
                array("ID", "CALLBACK_FUNC", "MODULE", "PRODUCT_ID", "QUANTITY", "PRODUCT_PROVIDER_CLASS")
            );
            while ($arItems = $dbBasketItems->Fetch()) {
                if ('' != $arItems['PRODUCT_PROVIDER_CLASS'] || '' != $arItems["CALLBACK_FUNC"]) {
                    CSaleBasket::UpdatePrice($arItems["ID"],
                        $arItems["CALLBACK_FUNC"],
                        $arItems["MODULE"],
                        $arItems["PRODUCT_ID"],
                        $arItems["QUANTITY"],
                        "N",
                        $arItems["PRODUCT_PROVIDER_CLASS"]
                    );
                    $arID[] = $arItems["ID"];
                }
            }
            if (!empty($arID)) {
                $dbBasketItems = CSaleBasket::GetList(
                    array(
                        "NAME" => "ASC",
                        "ID" => "ASC"
                    ),
                    array(
                        "ID" => $arID,
                        "ORDER_ID" => "NULL"
                    ),
                    false,
                    false,
                    array("ID", "CALLBACK_FUNC", "MODULE", "PRODUCT_ID", "QUANTITY", "DELAY", "CAN_BUY", "PRICE", "WEIGHT", "PRODUCT_PROVIDER_CLASS", "NAME")
                );
                while ($arItems = $dbBasketItems->Fetch()) {
                    $arBasketItems[] = $arItems;
                }
            }
// Печатаем массив, содержащий актуальную на текущий момент корзину
            echo "<pre>";
            print_r($arBasketItems);
            echo "</pre>";
        } else {
            $answer = array('status' => false, 'msg' => 'Wrong user ID');
        }

    }
}
?>