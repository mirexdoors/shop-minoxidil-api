<?
if ($_RAW['type'] == 'video') {
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
    CModule::IncludeModule("iblock");

    //Кэширование
    $obCache = new CPHPCache();

    $cache_time = 3600 * 24 * 14;
    $cacheID = serialize(array($_RAW['type'], $_RAW['number'], $_RAW['start']));
    $cachePath = '/mobile/video';

    if ($obCache->InitCache($cache_time, $cacheID, $cachePath))// Если кэш валиден
    {
        $vars = $obCache->GetVars();
        $answer = $vars['answer'];

    } elseif ($obCache->StartDataCache()) {

        $IBLOCK_ID = 14;
        $arResult = array();

        $arSelect = Array("ID", "NAME", "PROPERTY_LINK");
        $arFilter = Array("IBLOCK_ID" => $IBLOCK_ID, "ACTIVE" => "Y");

        $arNavStartParams = false;

        if (!isset($_RAW['start'])) {

            //параметр количества новостей
            if (isset($_RAW['number']) && !empty($_RAW['number'])) {
                $arNavStartParams = array("nTopCount" => intval($_RAW['number']));
            }

            $res = CIBlockElement::GetList(Array("ID" => "DESC"), $arFilter, false, $arNavStartParams, $arSelect);
            $i = 0;
            while ($ob = $res->GetNext()) {
                $arResult[$i]['ID'] = $ob['ID'];
                $arResult[$i]['NAME'] = $ob['NAME'];
                $arResult[$i]['PROPERTY_LINK_VALUE'] = $ob['PROPERTY_LINK_VALUE'];
                $i++;
            }

        } elseif (!empty($_RAW['start'])) {

            $res = CIBlockElement::GetList(Array("ID" => "DESC"), $arFilter, false, $arNavStartParams, $arSelect);
            $i = 0;
            while ($ob = $res->GetNext()) {

                $arResult[$i]['ID'] = $ob['ID'];
                $arResult[$i]['NAME'] = $ob['NAME'];
                $arResult[$i]['PROPERTY_LINK_VALUE'] = $ob['PROPERTY_LINK_VALUE'];

                $i++;
            }


            //Параметр старта выборки
            $amountElement = PHP_INT_MAX;
            if (isset($_RAW['number']) && !empty($_RAW['number'])) {
                $amountElement = intval($_RAW['number']);
            }

            foreach ($arResult as $arKey => $arItem) {
                $index = $arKey + 1;
                if ($index < intval($_RAW['start']) || $index >= (intval($_RAW['start']) + $amountElement)) {
                    unset($arResult[$arKey]);
                }
            }
        }

        if (!empty($arResult)) {
            $answer = $arResult;
            $obCache->EndDataCache(// Сохраняем переменные в кэш.
                array('answer' => $answer)
            );
        } else {
            $answer = array('status' => false, 'msg' => 'I have no videos');
        }
    }
}
?>

