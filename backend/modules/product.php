<?
if ($_RAW['type'] == 'product') {
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
    CModule::IncludeModule("iblock");

    if (isset($_RAW['id']) && !empty($_RAW['id'])) {

        //Кэширование
        $obCache = new CPHPCache();

        $cache_time = 3600 * 24 * 14;
        $cacheID = serialize(array($_RAW['type'], $_RAW['id']));
        $cachePath = '/mobile/product';

        if ($obCache->InitCache($cache_time, $cacheID, $cachePath))// Если кэш валиден
        {
            $vars = $obCache->GetVars();
            $answer = $vars['answer'];

        } elseif ($obCache->StartDataCache()) {

            $IBLOCK_ID = 2;
            $id = intval($_RAW['id']);

            $arResult = array();
            $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DETAIL_PICTURE", "DETAIL_TEXT", "IBLOCK_SECTION_ID", "PROPERTY_LIDER", "PROPERTY_ACTION", "PROPERTY_NEW", "PROPERTY_MORE_PHOTO", "CATALOG_GROUP_1");
            $arFilter = Array("IBLOCK_ID" => $IBLOCK_ID, "ID" => $id, "ACTIVE" => "Y");
            $res = CIBlockElement::GetList(Array("ID" => "DESC"), $arFilter, false, false, $arSelect);
            if ($ob = $res->GetNextElement()) {

                $arResult = array();
                $arItem = $ob->GetFields();
                $arItem += $ob->GetProperties();

                if (!empty($arItem['DETAIL_PICTURE'])) {
                    $src = CFile::GetPath($arItem['DETAIL_PICTURE']);
                    $arItem['DETAIL_PICTURE'] = $src;
                }

                //скидка
                $discount = null;
                $arDiscounts = CCatalogDiscount::GetDiscountByProduct($arItem['ID'], Array(),"N", array(), SITE_ID);

                if (isset($arDiscounts)) {
                    foreach ($arDiscounts as $arDisc) {
                        $discount = $arDisc['VALUE'];
                        $arItem['CATALOG_PRICE_1'] = $arItem['CATALOG_PRICE_1'] - $discount;
                    }
                }

                $arResult['DISCOUNT'] = $discount;

                if ($arItem['MORE_PHOTO']) {
                    $i = 0;
                    foreach ($arItem['MORE_PHOTO']['VALUE'] as $key => $value) {
                        $src = CFile::GetPath($value);
                        $arItem['MORE_PHOTO']['SRC'][$i] = $src;
                        $i++;
                    };
                }

                $arResult['ID'] = $arItem['ID'];
                $arResult['NAME'] = $arItem['NAME'];
                $arResult['DETAIL_PICTURE'] = $arItem['DETAIL_PICTURE'];
                $arResult['DETAIL_TEXT'] = html_entity_decode(strip_tags($arItem['DETAIL_TEXT']));
                $arResult['DATE_CREATE'] = $arItem['DATE_CREATE'];
                $arResult['PROPERTY_ACTION_VALUE'] = $arItem['PROPERTY_ACTION_VALUE'];
                $arResult['PROPERTY_LIDER_VALUE'] = $arItem['PROPERTY_LIDER_VALUE'];
                $arResult['PROPERTY_NEW_VALUE'] = $arItem['PROPERTY_NEW_VALUE'];
                $arResult['PROPERTY_MORE_PHOTO'] =  $arItem['MORE_PHOTO']['SRC'];
                $arResult['IBLOCK_SECTION_ID'] = $arItem['IBLOCK_SECTION_ID'];
                $arResult['PRICE'] = $arItem['CATALOG_PRICE_1'];
            }

            //цена/ Если нет модуля "Торговый каталог"
            /*$arResult['PRICE'] = null;
            $rsPrices = CPrice::GetList(array(), array('PRODUCT_ID' => $arResult['ID'], 'CATALOG_GROUP_ID' => 1));
            if ($arPrice = $rsPrices->GetNext()) {
                if (!empty($arResult['DISCOUNT'])) $arPrice["PRICE"] = $arPrice["PRICE"] - $discount;
                $arResult['PRICE'] = $arPrice["PRICE"];
            }*/


            $arSectionSelect = array('ID', 'NAME');
            $nav = CIBlockSection::GetNavChain($IBLOCK_ID, $arResult['IBLOCK_SECTION_ID'], $arSectionSelect);

            while ($arSectionPath = $nav->GetNext()) {

                $arSections[] = $arSectionPath;

            }

            foreach ($arSections as $secKey => $section) {
                foreach ($section as $key => $sec) {
                    if (strpos($key, '~') !== false) {
                        unset($arSections[$secKey][$key]);
                    }
                }
            }

            $arResult['SECTIONS'] = $arSections;

            if (!empty($arResult)) {
                $answer = $arResult;
                $obCache->EndDataCache(// Сохраняем переменные в кэш.
                    array('answer' => $answer)
                );
            } else {
                $answer = array('status' => false, 'msg' => 'I have no product with this ID');
            }
        }

    } else {
        $answer = array('status' => false, 'msg' => 'BAD query. No product ID');
    }
}

?>