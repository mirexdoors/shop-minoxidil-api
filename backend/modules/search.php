<? if ($_RAW['type'] == 'search') {
    if (isset($_RAW['q'])) {
        require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

        //Кэширование
        $obCache = new CPHPCache();

        $cache_time = 3600 * 24 * 14;
        $cacheID = serialize(array($_RAW['type'], $_RAW['q']));
        $cachePath = '/mobile/search';

        if ($obCache->InitCache($cache_time, $cacheID, $cachePath))// Если кэш валиден
        {
            $vars = $obCache->GetVars();
            $answer = $vars['answer'];

        } elseif ($obCache->StartDataCache()) {


            if (CModule::IncludeModule('search')) {
                $q = $_RAW['q'];
                $module_id = "iblock";
                $param1 = 'catalog'; //тип инфоблока
                $param2 = 2; //id инфоблока

                $obSearch = new CSearch;
                $obSearch->Search(array(
                    "QUERY" => $q,
                    "SITE_ID" => LANG,
                    "MODULE_ID" => $module_id,
                    "PARAM1" => $param1,
                    "PARAM2" => $param2,
                ));
                $i = 0;
                while ($arItems = $obSearch->GetNext()) {
                    $arResult[$i]['ID'] = $arItems['ID'];
                    $arResult[$i]['TITLE'] = $arItems['TITLE'];
                    $i++;
                }
            }
            if (!empty($arResult)) {
                $answer = $arResult;
                $obCache->EndDataCache(// Сохраняем переменные в кэш.
                    array('answer' => $answer)
                );
            } else {
                $answer = array('status' => false, 'msg' => 'I can`t found this');
            }
        }
    } else {
        $answer = array('status' => false, 'msg' => 'I have no search query');
    }
}
?>