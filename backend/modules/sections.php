<? if ($_RAW['type'] == 'sections') {
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
    CModule::IncludeModule("iblock");

    //Кэширование
    $obCache = new CPHPCache();

    $cache_time = 3600 * 24 * 14;
    $cacheID = serialize(array($_RAW['type']));
    $cachePath = '/mobile/sections';

    if ($obCache->InitCache($cache_time, $cacheID, $cachePath)) {
        $vars = $obCache->GetVars();
        $answer = $vars['answer'];

    } elseif ($obCache->StartDataCache()) {
        $IBLOCK_ID = 2;
        $arFilter = array(
            'ACTIVE' => 'Y',
            'IBLOCK_ID' => $IBLOCK_ID,
            'GLOBAL_ACTIVE' => 'Y',
        );
        $arSelect = array('ID', 'NAME', 'DEPTH_LEVEL', 'IBLOCK_SECTION_ID');
        $arOrder = array('DEPTH_LEVEL' => 'ASC');
        $rsSections = CIBlockSection::GetList($arOrder, $arFilter, false, $arSelect);
        $sectionLinc = array();
        $arResult['ROOT'] = array();
        $sectionLinc[0] = &$arResult['ROOT'];
        while ($arSection = $rsSections->GetNext()) {
            $sectionLinc[intval($arSection['IBLOCK_SECTION_ID'])]['CHILD'][$arSection['ID']] = $arSection;
            $sectionLinc[$arSection['ID']] = &$sectionLinc[intval($arSection['IBLOCK_SECTION_ID'])]['CHILD'][$arSection['ID']];
        }
        unset($sectionLinc);

        if (!empty($arResult)) {
            $answer = $arResult;
            $obCache->EndDataCache(// Сохраняем переменные в кэш.
                array('answer' => $answer)
            );
        } else {
            $answer = array('status' => false, 'msg' => 'I have no sections');
        }
    }
}
?>
