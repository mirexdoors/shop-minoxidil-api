<? if
($_RAW['type'] == 'catalog') {

    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
    CModule::IncludeModule("iblock");

    $IBLOCK_ID = 2;
    $arResult = array();

    $arSelect = Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE", "DETAIL_PICTURE", "IBLOCK_SECTION_ID", "PROPERTY_LIDER", "PROPERTY_ACTION", "PROPERTY_NEW", "CATALOG_GROUP_1");


    //фильтрация по свойствам
    $arFilter = Array("IBLOCK_ID" => $IBLOCK_ID, "ACTIVE" => "Y");

    if (isset($_RAW['filters']) && !empty($_RAW['filters'])) {
        $activeFilters = array();
        foreach ($_RAW['filters'] as $filter) {
            if ($filter) $activeFilters[] = $filter;
        }
        foreach ($activeFilters as $activeFilter) {
            $arFilter = Array('!' . $ARFILTERS[$activeFilter] => false);
        }
    }

    $arNavStartParams = false;

    if (!isset($_RAW['start'])) {

        //параметр количества новостей
        if (isset($_RAW['number']) && !empty($_RAW['number'])) {
            $arNavStartParams = array("nTopCount" => intval($_RAW['number']));
        }


        $res = CIBlockElement::GetList(Array("ID" => "DESC"), $arFilter, false, $arNavStartParams, $arSelect);

        $i = 0;
        while ($ob = $res->GetNext()) {

            //изображение
            if (!empty($ob['PREVIEW_PICTURE'])) {
                $src = CFile::GetPath($ob['PREVIEW_PICTURE']);
                $ob['PREVIEW_PICTURE'] = $src;
            } elseif (!empty($ob['DETAIL_PICTURE'])) {
                $src = CFile::GetPath($ob['DETAIL_PICTURE']);
                $ob['PREVIEW_PICTURE'] = $src;
            }

            //скидка
            $discount = null;
            $arDiscounts = CCatalogDiscount::GetDiscountByProduct($ob['ID'], Array(), "N", array(), SITE_ID);

            if (isset($arDiscounts)) {
                foreach ($arDiscounts as $arDisc) {
                    $discount = $arDisc['VALUE'];
                    $ob['CATALOG_PRICE_1'] = $ob['CATALOG_PRICE_1'] - $discount;
                }
            }


            $arResult[$i]['ID'] = $ob['ID'];
            $arResult[$i]['NAME'] = $ob['NAME'];
            $arResult[$i]['PREVIEW_PICTURE'] = $ob['PREVIEW_PICTURE'];
            $arResult[$i]['PREVIEW_TEXT'] = html_entity_decode(strip_tags($ob['PREVIEW_TEXT']));
            $arResult[$i]['PROPERTY_ACTION_VALUE'] = $ob['PROPERTY_ACTION_VALUE'];
            $arResult[$i]['PROPERTY_LIDER_VALUE'] = $ob['PROPERTY_LIDER_VALUE'];
            $arResult[$i]['PROPERTY_NEW_VALUE'] = $ob['PROPERTY_NEW_VALUE'];
            $arResult[$i]['IBLOCK_SECTION_ID'] = $ob['IBLOCK_SECTION_ID'];
            $arResult[$i]['PRICE'] = intval($ob['CATALOG_PRICE_1']);
            $arResult[$i]['DISCOUNT'] = intval($discount);

            //все разделы элемента
            $arSections = array();
            $arSectionSelect = array('ID', 'NAME');
            $nav = CIBlockSection::GetNavChain($IBLOCK_ID, $arResult[$i]['IBLOCK_SECTION_ID'], $arSectionSelect);

            while ($arSectionPath = $nav->GetNext()) {

                $arSections[] = $arSectionPath;

            }

            foreach ($arSections as $secKey => $section) {
                foreach ($section as $key => $sec) {
                    if (strpos($key, '~') !== false) {
                        unset($arSections[$secKey][$key]);
                    }
                }
            }

            $arResult[$i]['SECTIONS'] = $arSections;

            //фильтрация по цене
            if (!empty($_RAW['priceFilter']['from'])) {
                $from = intval($_RAW['priceFilter']['from']);
                if ($arResult[$i]['PRICE'] <= $from) {
                    unset($arResult[$i]);
                }
            }

            if (!empty($_RAW['priceFilter']['to'])) {
                $to = intval($_RAW['priceFilter']['to']);
                if ($arResult[$i]['PRICE'] <= $to) {
                    unset($arResult[$i]);
                }
            }

            //Фильтрация по разделу
            if (!empty($_RAW['section'])) {
                foreach ($arResult[$i]['SECTIONS'] as $section) {
                    if (in_array($section['ID'], $_RAW['section']) == false) {
                        unset($arResult[$i]);
                    }
                }
            }

            $i++;
        }
    } elseif (!empty($_RAW['start'])) {

        $res = CIBlockElement::GetList(Array("ID" => "DESC"), $arFilter, false, $arNavStartParams, $arSelect);
        $i = 0;
        while ($ob = $res->GetNext()) {
            //изображение
            if (!empty($ob['PREVIEW_PICTURE'])) {
                $src = CFile::GetPath($ob['PREVIEW_PICTURE']);
                $ob['PREVIEW_PICTURE'] = $src;
            } elseif (!empty($ob['DETAIL_PICTURE'])) {
                $src = CFile::GetPath($ob['DETAIL_PICTURE']);
                $ob['PREVIEW_PICTURE'] = $src;
            }

            //скидка
            $discount = null;
            $arDiscounts = CCatalogDiscount::GetDiscountByProduct($ob['ID'], Array(), "N", array(), SITE_ID);

            if (isset($arDiscounts)) {
                foreach ($arDiscounts as $arDisc) {
                    $discount = $arDisc['VALUE'];
                    $ob['CATALOG_PRICE_1'] = $ob['CATALOG_PRICE_1'] - $discount;
                }
            }


            $arResult[$i]['ID'] = $ob['ID'];
            $arResult[$i]['NAME'] = $ob['NAME'];
            $arResult[$i]['PREVIEW_PICTURE'] = $ob['PREVIEW_PICTURE'];
            $arResult[$i]['PREVIEW_TEXT'] = html_entity_decode(strip_tags($ob['PREVIEW_TEXT']));
            $arResult[$i]['PROPERTY_ACTION_VALUE'] = $ob['PROPERTY_ACTION_VALUE'];
            $arResult[$i]['PROPERTY_LIDER_VALUE'] = $ob['PROPERTY_LIDER_VALUE'];
            $arResult[$i]['PROPERTY_NEW_VALUE'] = $ob['PROPERTY_NEW_VALUE'];
            $arResult[$i]['IBLOCK_SECTION_ID'] = $ob['IBLOCK_SECTION_ID'];
            $arResult[$i]['PRICE'] = intval($ob['CATALOG_PRICE_1']);
            $arResult[$i]['DISCOUNT'] = intval($discount);

            //все разделы элемента
            $arSections = array();
            $arSectionSelect = array('ID', 'NAME');
            $nav = CIBlockSection::GetNavChain($IBLOCK_ID, $arResult[$i]['IBLOCK_SECTION_ID'], $arSectionSelect);

            while ($arSectionPath = $nav->GetNext()) {

                $arSections[] = $arSectionPath;

            }

            foreach ($arSections as $secKey => $section) {
                foreach ($section as $key => $sec) {
                    if (strpos($key, '~') !== false) {
                        unset($arSections[$secKey][$key]);
                    }
                }
            }

            $arResult[$i]['SECTIONS'] = $arSections;

            //фильтрация по цене
            if (!empty($_RAW['priceFilter']['from'])) {
                $from = intval($_RAW['priceFilter']['from']);
                if ($arResult[$i]['PRICE'] <= $from) {
                    unset($arResult[$i]);
                }
            }

            if (!empty($_RAW['priceFilter']['to'])) {
                $to = intval($_RAW['priceFilter']['to']);
                if ($arResult[$i]['PRICE'] <= $to) {
                    unset($arResult[$i]);
                }
            }

            //Фильтрация по разделу
            if (!empty($_RAW['section'])) {
                foreach ($arResult[$i]['SECTIONS'] as $section) {
                    if (in_array($section['ID'], $_RAW['section']) == false) {
                        unset($arResult[$i]);
                    }
                }
            }

            $i++;
        }

        //Параметр старта выборки
        $amountElement = PHP_INT_MAX;
        if (isset($_RAW['number']) && !empty($_RAW['number'])) {
            $amountElement = intval($_RAW['number']);
        }

        foreach ($arResult as $arKey => $arItem) {
            $index = $arKey + 1;
            if ($index < intval($_RAW['start']) || $index >= (intval($_RAW['start']) + $amountElement)) {
                unset($arResult[$arKey]);
            }
        }
    }
    $answer = $arResult;
} ?>
