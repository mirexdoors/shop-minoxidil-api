<?
include('config/headers.php');


$_RAW =  json_decode(file_get_contents('php://input'), true);
   if (!empty($_POST)) {
       $_RAW = $_POST;
   }

if ($_RAW['token'] == 'appsweb') {
    include('config/headers.php');
    include("const/catalog_filters.php");
    include('modules/news.php');
    include('modules/news_detail.php');
    include('modules/video.php');
    include('modules/product.php');
    include('modules/catalog.php');
    include('modules/basket.php');
    include('modules/sections.php');
    include('modules/search.php');
    include('config/headers.php');

} else {
    $answer = array('status' => false, 'msg' => 'Bad token');
}
echo json_encode($answer);

?>