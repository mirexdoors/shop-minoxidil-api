# shop-minoxidil API
`https://shop-minoxidil.ru/mobile/api/backend/api.php`


## `GET`-запросы

| Имя | Тип данных      | Варианты | Описание |
| -------- | -------- | -------- | -------- |
| type     | string     | `news` - новости,<br>`news_detail` - детальная новость (нужен `id` новости)| Тип отдаваемого контента     |
| id     | int     | | идентификационный номер единицы контента     |

