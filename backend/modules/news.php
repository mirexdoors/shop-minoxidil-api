<?
if ($_RAW['type'] == 'news') {
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
    CModule::IncludeModule("iblock");

    //Кэширование
    $obCache = new CPHPCache();
    $cache_time = 3600 * 24 * 14;
    $cacheID = serialize(array($_RAW['type'], $_RAW['number'], $_RAW['start']));
    $cachePath = '/mobile/news';

    if ($obCache->InitCache($cache_time, $cacheID, $cachePath))// Если кэш валиден
    {
        $vars = $obCache->GetVars();
        $answer = $vars['answer'];


    } elseif ($obCache->StartDataCache()) {

        $IBLOCK_ID = 1;
        $arResult = array();

        $arSelect = Array("ID", "NAME", "PREVIEW_TEXT", "PREVIEW_PICTURE", "DATE_CREATE");
        $arFilter = Array("IBLOCK_ID" => $IBLOCK_ID, "ACTIVE" => "Y");

        $arNavStartParams = false;

        if (!isset($_RAW['start'])) {

            //параметр количества новостей
            if (isset($_RAW['number']) && !empty($_RAW['number'])) {
                $arNavStartParams = array("nTopCount" => intval($_RAW['number']));
            }

            $res = CIBlockElement::GetList(Array("ID" => "DESC"), $arFilter, false, $arNavStartParams, $arSelect);

            $i = 0;
            while ($ob = $res->GetNext()) {

                if (!empty($ob['PREVIEW_PICTURE'])) {
                    $src = CFile::GetPath($ob['PREVIEW_PICTURE']);
                    $ob['PREVIEW_PICTURE'] = $src;
                } elseif (!empty($ob['DETAIL_PICTURE'])) {
                    $src = CFile::GetPath($ob['DETAIL_PICTURE']);
                    $ob['PREVIEW_PICTURE'] = $src;
                }

                $arResult[$i]['ID'] = $ob['ID'];
                $arResult[$i]['NAME'] = $ob['NAME'];
                $arResult[$i]['PREVIEW_PICTURE'] = $ob['PREVIEW_PICTURE'];
                $arResult[$i]['PREVIEW_TEXT'] = html_entity_decode(strip_tags($ob['PREVIEW_TEXT']));
                $arResult[$i]['DATE_CREATE'] = $ob['DATE_CREATE'];
                $i++;
            }


        } elseif (!empty($_RAW['start'])) {

            $res = CIBlockElement::GetList(Array("ID" => "DESC"), $arFilter, false, $arNavStartParams, $arSelect);

            $i = 0;
            while ($ob = $res->GetNext()) {

                if (!empty($ob['PREVIEW_PICTURE'])) {
                    $src = CFile::GetPath($ob['PREVIEW_PICTURE']);
                    $ob['PREVIEW_PICTURE'] = $src;
                } elseif (!empty($ob['DETAIL_PICTURE'])) {
                    $src = CFile::GetPath($ob['DETAIL_PICTURE']);
                    $ob['PREVIEW_PICTURE'] = $src;
                }


                $arResult[$i]['ID'] = $ob['ID'];
                $arResult[$i]['NAME'] = $ob['NAME'];
                $arResult[$i]['PREVIEW_PICTURE'] = $ob['PREVIEW_PICTURE'];
                $arResult[$i]['PREVIEW_TEXT'] = html_entity_decode(strip_tags($ob['PREVIEW_TEXT']));
                $arResult[$i]['DATE_CREATE'] = $ob['DATE_CREATE'];

                $i++;
            }


            //Параметр старта выборки
            $amountElement = PHP_INT_MAX;
            if (isset($_RAW['number']) && !empty($_RAW['number'])) {
                $amountElement = intval($_RAW['number']);
            }

            foreach ($arResult as $arKey => $arItem) {
                $index = $arKey + 1;
                if ($index < intval($_RAW['start']) || $index >= (intval($_RAW['start']) + $amountElement)) {
                    unset($arResult[$arKey]);
                }
            }

        }

        if (!empty($arResult)) {
            $answer = $arResult;
            $obCache->EndDataCache(// Сохраняем переменные в кэш.
                array('answer' => $answer)
            );
        } else {
            $answer = array('status' => false, 'msg' => 'I have no news');
        }
    }
}
?>

